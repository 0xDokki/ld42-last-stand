class Preloader {
    init() {
        Number.prototype.clamp = function(min, max) {
            return this <= min ? min : this >= max ? max : this;
        };

        Phaser.Math.Vector2.prototype.angleDegrees = function() {
            return (this.angle() * 180) / Math.PI;
        };
    }

    preload() {
        this.load.bitmapFont(
            R.font.mario,
            "assets/nummernschild_0.png",
            "assets/nummernschild.fnt"
        );

        this.load.atlas(R.image.atlas_rts, "assets/rts.png", "assets/rts.json");
        this.load.atlas(
            R.image.atlas_tanks,
            "assets/tanks.png",
            "assets/tanks.json"
        );
        this.load.atlas(R.image.atlas_td, "assets/td.png", "assets/td.json");

        this.load.image(R.image.tileset_1, "assets/tiles_1.png");

        this.load.image(R.image.header, "assets/header.png");
        this.load.image(R.image.particle, "assets/particle.png");
    }

    create() {
        this.scene.start(R.state.mainmenu);
    }
}
