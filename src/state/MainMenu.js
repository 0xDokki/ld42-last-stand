class MainMenu {
    create() {
        this.title = this.add.sprite(320, 288, R.image.header);

        this.textStartGame = this.add.bitmapText(
            130,
            420,
            R.font.mario,
            "Start Game",
            45
        );
        this.textStartGame.setOrigin(0.5, 0.5);

        this.textTween = this.tweens.addCounter({
            from: -5,
            to: 5,
            duration: 750,
            yoyo: true,
            repeat: -1
        });
    }

    update(time, delta) {
        this.textStartGame.setAngle(this.textTween.getValue());

        let rect = new Phaser.Geom.Rectangle(
            this.textStartGame.x - this.textStartGame.width / 2,
            this.textStartGame.y - this.textStartGame.height / 2,
            this.textStartGame.width,
            this.textStartGame.height
        );
        let intersect1 = rect.contains(this.input.x, this.input.y);
        if (!intersect1 && this.textTween.isPaused()) {
            this.textStartGame.fontSize = 45;
            this.textTween.resume();
        } else if (intersect1 && this.textTween.isPlaying()) {
            this.textTween.pause();
            this.textStartGame.fontSize = 55;
        }

        if (intersect1 && this.input.activePointer.primaryDown)
            this.scene.start(R.state.maingame);
    }
}
